<?php

namespace Drupal\nls_admin_lock\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * The widget for the Admin Lock field.
 *
 * @FieldWidget(
 *   id = "nls_admin_lock",
 *   label = @Translation("Admin Lock"),
 *   field_types = {
 *     "nls_admin_lock",
 *   }
 * )
 */
class AdminLockWidget extends WidgetBase {

  /**
   * Add the fields to the form.
   *
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items->get($delta);
    $elements = [];
    if (\Drupal::currentUser()->hasPermission('admin_nls_admin_lock')) {
      $elements = [
        '#type' => 'details',
        '#title' => t('Admin lock'),
        '#group' => 'advanced',
        '#open' => !$item->isEmpty(),
      ];
      $elements['admin_lock']['lock_update'] = [
        '#type' => 'checkbox',
        '#title' => t('Lock for updates'),
        '#default_value' => $item->get('lock_update')->getValue(),
        '#description' => t('Lock this entity so only administrators can update it'),
      ];
      $elements['admin_lock']['lock_delete'] = [
        '#type' => 'checkbox',
        '#title' => t('Lock for deletion'),
        '#default_value' => $item->get('lock_delete')->getValue(),
        '#description' => t('Lock this entity so only administrators can delete it'),
      ];
      $elements['admin_lock']['lock_menu'] = [
        '#type' => 'checkbox',
        '#title' => t('Lock menu settings'),
        '#default_value' => $item->get('lock_menu')->getValue(),
        '#description' => t('Lock this entity so only administrators can change the menu settings'),
      ];
      $elements['admin_lock']['lock_path'] = [
        '#type' => 'checkbox',
        '#title' => t('Lock path settings'),
        '#default_value' => $item->get('lock_path')->getValue(),
        '#description' => t('Lock this entity so only administrators can change the path settings'),
      ];
    }
    return $elements;
  }

  /**
   * Put the element values in the expected format so they can be saved to the database.
   *
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $result = [];
    foreach ($values as $item) {
      foreach ($item['admin_lock'] as $key => $value) {
        $result[$key] = $value;
      }
    }
    return $result;
  }

}