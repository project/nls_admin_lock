<?php

namespace Drupal\nls_admin_lock\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * The formatter for the Admin Lock field.
 *
 * @FieldFormatter(
 *   id = "nls_admin_lock_empty_formatter",
 *   module = "nls_admin_lock",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "nls_admin_lock"
 *   }
 * )
 */
class AdminLockFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [];
  }

}
