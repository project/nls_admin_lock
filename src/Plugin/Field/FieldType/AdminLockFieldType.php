<?php

namespace Drupal\nls_admin_lock\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * The field type that enables the Admin Lock.
 *
 * @FieldType(
 *   id = "nls_admin_lock",
 *   label = @Translation("Admin Lock fields"),
 *   default_formatter = "nls_admin_lock_empty_formatter"
 * )
 */
class AdminLockFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'lock_update' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'lock_delete' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'lock_menu' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
        'lock_path' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return !$this->get('lock_update')->getValue() && !$this->get('lock_delete')->getValue() && !$this->get('lock_menu')->getValue() && !$this->get('lock_path')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['lock_update'] = DataDefinition::create('integer')->setLabel('Update');
    $properties['lock_delete'] = DataDefinition::create('integer')->setLabel('Delete');
    $properties['lock_menu'] = DataDefinition::create('integer')->setLabel('Menu');
    $properties['lock_path'] = DataDefinition::create('integer')->SetLabel('Path');
    return $properties;
  }

}