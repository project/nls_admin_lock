CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

https://www.drupal.org/sandbox/markwittens/2854814

REQUIREMENTS
------------
No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

 * Navigate to /admin/people/permissions and enable the "Administer Admin Lock" permission for the roles who need to be able to lock and unlock pieces of content
 * Add an Admin Lock field to the content type where you need it
 * Edit an entity of the content type you just added the Admin Lock field to, you will notice some new options:
   - Lock for updates: Lock this entity so only administrators can update it
   - Lock for deletion: Lock this entity so only administrators can delete it
   - Lock menu settings: Lock this entity so only administrators can change the menu settings
   - Lock path settings: Lock this entity so only administrators can change the path settings
 * Check the boxes you need to lock for the entity
 * You are done! Users without the "Administer Admin Lock" permission are now unable to edit the selected sections of the entity.

MAINTAINERS
-----------

Current maintainer:
  * Mark Wittens (markwittens) - https://www.drupal.org/user/567198

This project has been sponsored by:
  * Nilsson - https://www.nilsson.nl
